package com.bms.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bms.entity.Accomodation;
import com.bms.entity.Category;
import com.bms.entity.District;
import com.bms.entity.Post;
import com.bms.entity.ReportPost;
import com.bms.services.AccomodationService;
import com.bms.services.CategoryService;
import com.bms.services.DistrictService;
import com.bms.services.PostService;
import com.bms.services.ReportPostService;
import com.bms.services.UserService;

@Controller
public class PostController {
	@Autowired
	DistrictService districtService;

	@Autowired
	CategoryService categoryService;

	@Autowired
	PostService postService;

	@Autowired
	ReportPostService reportPostService;

	@Autowired
	UserService userService;
	
	@Autowired
	AccomodationService accomodationService;

	@RequestMapping({ "/view-posts", "/home" })
	public String viewPage(Model model, @Param("minPrice") String minPrice, @Param("maxPrice") String maxPrice,
			@Param("minArea") String minArea, @Param("maxArea") String maxArea, @Param("districtId") String districtId,
			@Param("categoryId") String categoryId, HttpSession session) {

		if (UserController.loggedAccount == null) {
			UserController.loggedAccount = UserController.getCurrentAccount();
			UserController.loggedUser = userService.findByAccount(UserController.loggedAccount).get();
		}

		session.setAttribute("user_fullName", UserController.loggedUser.getFullName());
		session.setAttribute("minPrice", minPrice);
		session.setAttribute("maxPrice", maxPrice);

		session.setAttribute("districtId", districtId);

		session.setAttribute("minArea", minArea);
		session.setAttribute("maxArea", maxArea);

		session.setAttribute("categoryId", categoryId);
		return viewPage(model, 1, session);
	}

	@RequestMapping("/list-post/page/{pageNum}")
	public String viewPage(Model model, @PathVariable(name = "pageNum") int pageNum, HttpSession session) {

		Category category = null;

		District district = null;

		List<Category> listCategories = categoryService.findAll();
		List<District> listDistricts = districtService.findAll();

		double minArea, maxArea, minPrice, maxPrice;
		minArea = maxArea = minPrice = maxPrice = 0;

		if (session.getAttribute("categoryId") != null) {
			category = categoryService.findById(Integer.parseInt(session.getAttribute("categoryId").toString()));
			district = districtService.findById(Integer.parseInt(session.getAttribute("districtId").toString()));

			minArea = Double.parseDouble(session.getAttribute("minArea").toString());
			maxArea = Double.parseDouble(session.getAttribute("maxArea").toString());
			minPrice = Double.parseDouble(session.getAttribute("minPrice").toString());
			maxPrice = Double.parseDouble(session.getAttribute("maxPrice").toString());
		}

		Page<Post> page = postService.findAllOrderByDesc(1, category, district, minArea, maxArea, minPrice, maxPrice);

		List<Post> listPosts = page.getContent();
		

		model.addAttribute("currentPage", pageNum);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("listPosts", listPosts);
		model.addAttribute("minArea", minArea);
		model.addAttribute("maxArea", maxArea);
		model.addAttribute("minPrice", minPrice);
		model.addAttribute("maxPrice", maxPrice);
		model.addAttribute("listCategories", listCategories);
		model.addAttribute("listDistricts", listDistricts);

		return "view-posts";
	}

	@GetMapping("add-post")
	public String addPostPageGet(Model model) {
		List<Category> listCategories = categoryService.findAll();
		List<District> listDistricts = districtService.findAll();
		model.addAttribute("listCategories", listCategories);
		model.addAttribute("listDistricts", listDistricts);
		return "add-post";
	}

	@PostMapping("add-post")
	public String addPostPagePost(Model model) {
		return "add-post";
	}

	// admin -approve post
	@GetMapping("/approve-post")
	public String approvePostPage(@Param("postId") long postId, Model model) {
		Post post = postService.findById(postId);
		post.setApprove(true);
		return "view-wait-aprove-post";
	}

	// admin -hide post
	@GetMapping("/hidden-post")
	public String hiddenPostPage(@Param("postId") long postId, Model model) {
		Post post = postService.findById(postId);
		post.setHidden(true);
		post.setApprove(false);
		return "view-hidden-post";
	}

	// admin-view all wait approve post
	@GetMapping("/all-list-post-wait-approve")
	public String listWaitApprovePost(Model model) {
		return viewlistWaitApprovePostPage(model, 1);
	}

	@GetMapping("/all-list-post-wait-approve/page/{pageNum}")
	public String viewlistWaitApprovePostPage(Model model, @PathVariable(name = "pageNum") int pageNum) {
		Page<Post> page = postService.findAllNonApprovePost(pageNum);
		List<Post> listPosts = page.getContent();

		model.addAttribute("currentPage", pageNum);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("listPosts", listPosts);
		return "view-wait-aprove-post-admin";
	}

	// admin-view all hidden post
	@GetMapping("/all-list-post-hidden")
	public String listHiddenPost(Model model) {
		return listHiddenPost(model, 1);
	}

	@GetMapping("/all-list-post-hidden/page/{pageNum}")
	public String listHiddenPost(Model model, @PathVariable(name = "pageNum") int pageNum) {
		Page<Post> page = postService.findAllHiddenPost(pageNum);
		List<Post> listPosts = page.getContent();

		model.addAttribute("currentPage", pageNum);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("listPosts", listPosts);
		return "view-hiddent-post-admin";
	}

	// admin-view all report post
	@GetMapping("/all-list-post-report")
	public String listReportPost(Model model) {
		return listReportPost(model, 1);
	}

	@GetMapping("/all-list-post-report/page/{pageNum}")
	public String listReportPost(Model model, @PathVariable(name = "pageNum") int pageNum) {
		Page<ReportPost> page = reportPostService.findAllReport(pageNum);
		List<ReportPost> listReportPosts = page.getContent();

		model.addAttribute("currentPage", pageNum);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("listReportPosts", listReportPosts);
		return "view-all-report-post";
	}

	// user own- view all wait post of user
	@GetMapping("/list-user-post-wait-approve")
	public String listWaitApprovePostOfUser(Model model) {
		return listWaitApprovePostOfUser(model, 1);
	}

	@GetMapping("/list-user-post-wait-approve/page/{pageNum}")
	public String listWaitApprovePostOfUser(Model model, @PathVariable(name = "pageNum") int pageNum) {
		Page<Post> page = postService.findWaitApproveUserPost(pageNum, UserController.loggedUser);
		List<Post> listPosts = page.getContent();
		System.out.println("da chay vao roif ma");

		model.addAttribute("currentPage", pageNum);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("listPosts", listPosts);
		return "view-wait-aprove-user-post";
	}

	// user own- view all approve post of user
	@GetMapping("/list-user-post-approved")
	public String listApprovePostOfUser(Model model) {
		return listApprovePostOfUser(model, 1);
	}

	@GetMapping("/list-user-post-approved/page/{pageNum}")
	public String listApprovePostOfUser(Model model, @PathVariable(name = "pageNum") int pageNum) {
		Page<Post> page = postService.findApproveUserPost(pageNum, UserController.loggedUser);
		List<Post> listPosts = page.getContent();

		model.addAttribute("currentPage", pageNum);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("listPosts", listPosts);
		return "view-aprove-post-user";
	}

	// user own- view all hidden post of user
	@GetMapping("/list-user-post-hidden")
	public String listHiddenPostOfUser(Model model) {
		return listHiddenPostOfUser(model, 1);
	}

	@GetMapping("/list-user-post-hidden/page/{pageNum}")
	public String listHiddenPostOfUser(Model model, @PathVariable(name = "pageNum") int pageNum) {
		Page<Post> page = postService.findHiddenUserPost(pageNum, UserController.loggedUser);
		List<Post> listPosts = page.getContent();

		model.addAttribute("currentPage", pageNum);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("listPosts", listPosts);
		return "view-user-hidden-post";
	}

	// user own- view all post of user
	@GetMapping("/list-user-post")
	public String listPostOfUser(Model model) {
		return listPostOfUser(model, 1);
	}

	@GetMapping("/list-user-post/page/{pageNum}")
	public String listPostOfUser(Model model, @PathVariable(name = "pageNum") int pageNum) {
		Page<Post> page = postService.findbyUser(pageNum, UserController.loggedUser);
		List<Post> listPosts = page.getContent();
		
		for (Post post : listPosts) {
			System.out.println(post.getPrice());
		}

		model.addAttribute("currentPage", pageNum);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("listPosts", listPosts);
		return "view-user-post";
	}

	@GetMapping("detail-post")
	public String detailPostPage(@Param("postId") long postId, Model model) {
		Post post = postService.findById(postId);
		Accomodation accomodation = accomodationService.findByPostid(post);
		model.addAttribute("post", post);
		model.addAttribute("accomodation", accomodation);
		return "detail-post";
	}
	
	// user own- view list save post of user
	@GetMapping("/list-user-save-post")
	public String listPosSavetOfUser(Model model) {
		List<Post> listPosts  = UserController.loggedUser.getPosts();
		
		for (Post post : listPosts) {
			System.out.println(post.getTitle());
		}
		model.addAttribute("listPosts", listPosts);
		return "list-save-post";
	}

	
}

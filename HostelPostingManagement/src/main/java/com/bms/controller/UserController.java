package com.bms.controller;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.bms.dao.AccountDetailsDTO;
import com.bms.entity.Account;
import com.bms.entity.Post;
import com.bms.entity.User;
import com.bms.repository.AccountRepository;
import com.bms.services.PostService;
import com.bms.services.UserService;

@Controller
public class UserController {

	public static Account loggedAccount;
	public static User loggedUser;

	@Autowired
	UserService userService;

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	PostService postService;

	private static final void validatePrinciple(Object principle) {
		if (!(principle instanceof AccountDetailsDTO)) {
			throw new IllegalArgumentException("Principle can not null");
		}
	}

	public static final Account getCurrentAccount() {
		UsernamePasswordAuthenticationToken authenticationToken = (UsernamePasswordAuthenticationToken) SecurityContextHolder
				.getContext().getAuthentication();

		validatePrinciple(authenticationToken.getPrincipal());
		return ((AccountDetailsDTO) authenticationToken.getPrincipal()).getAccountDetails();
	}

	@GetMapping("/user-profile")
	public String userProfile(Model model) {
		model.addAttribute("user", loggedUser);
		return "user-profile";
	}

	@PostMapping("/edit-profile")
	public String editProfilePage(User user) {
		String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
		user.setAccount(loggedUser.getAccount());
		user.setCreatedDate(loggedUser.getCreatedDate());
		user.setUpdateDate(timeStamp);
		user.setId(loggedUser.getId());

		boolean isResult = userService.insertUser(user);
		if (isResult) {
			return "redirect:/home";
		}
		return "redirect:/add-content-page";
	}

	@GetMapping("/reset-password")
	public String resetPaswordPageGet(Model model) {
		return "resetPasword";
	}

	@PostMapping("/reset-password")
	public String resetPaswordPagePost(Account account, @Param("passwordOld") String passwordOld, Model model) {
		int strength = 10;
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(strength, new SecureRandom());
		String encodedPasswordOld = bCryptPasswordEncoder.encode(passwordOld);

		boolean isPasswordMatch = loggedAccount.getPassword().equals(encodedPasswordOld);
		if (!isPasswordMatch) {
			model.addAttribute("error", "Mật khẩu cũ không trùng khớp, vui lòng thử lại!");
		}
		String encodedPasswordNew = bCryptPasswordEncoder.encode(account.getPassword());
		
		account.setPassword(encodedPasswordNew);
		account.setBlock(false);
		account.setUsername(loggedAccount.getUsername());
		
		boolean isUpdateAccount = accountRepository.save(account) != null;
		
		if(!isUpdateAccount) {
			model.addAttribute("error", "Đã xảy ra lỗi, vui lòng thử lại!");
			return "reset-password";
		}

		return "redirect:/reset-password";
	}
	
	@GetMapping("/block-user")
	public String blockUserPage(@Param("userId") long userId, Model model) {
		User user = userService.findById(userId).get();
		Account account = user.getAccount();
		account.setBlock(true);
		boolean isBlockAccount = accountRepository.save(account)!= null;
		if(!isBlockAccount) {
			model.addAttribute("result","Khóa user thất bại!");
			return "view-users";
		}
		model.addAttribute("result","Khóa user thành công!");
		return "view-users";
	}
	
	// admin-view all user
	@GetMapping("/all-user")
	public String listUserPage(Model model) {
		return listUserpage(model, 1);
	}

	@GetMapping("/view-user/page/{pageNum}")
	public String listUserpage(Model model, @PathVariable(name = "pageNum") int pageNum) {
		Page<User> page = userService.listAll(pageNum);
		List<User> listUsers = page.getContent();

		model.addAttribute("currentPage", pageNum);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("listProducts", listUsers);
		return "view-user";
	}
	
	@GetMapping("/add-save-post")
	public void addSavePostPage(@Param("postId") long postId) {
		List<Post> posts = loggedUser.getPosts();
		
		for (Post post : posts) {
			if(post.getId() == postId) {
				loggedUser.removeLikeRole(post);
			}
			else {
				Post postTemp = postService.findById(postId);
				loggedUser.addLikePost(postTemp);			}
		}
	}
}

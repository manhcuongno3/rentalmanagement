package com.bms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bms.entity.Category;
import com.bms.entity.District;
import com.bms.entity.Post;
import com.bms.entity.User;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

	Page<Post> findByIsApproveTrueOrderByIdDesc(Pageable pageable);

	Page<Post> findByDistrictAndCategoryAndAreaBetweenAndPriceBetweenAndIsApproveTrueOrderByIdDesc(District district, Category category,
			Double startArea, Double endArea, Double startPrice, Double endPrice, Pageable pageable);

	// list all post wait to approve
	Page<Post> findByIsApproveFalseAndIsHiddenFalse(Pageable pageable);

	// list all post is block
	Page<Post> findByIsHiddenTrue(Pageable pageable);

	// list post of user wait to approve
	Page<Post> findByUserAndIsApproveFalse(User user, Pageable pageable);
	
	// list post of user is approved
	Page<Post> findByUserAndIsApproveTrue(User user,Pageable pageable);

	// list post of user block
	Page<Post> findByUserAndIsHiddenTrue(User user, Pageable pageable);
	
	//list post of user
	Page<Post> findByUser(User user, Pageable pageable);
}

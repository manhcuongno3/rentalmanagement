package com.bms.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;

import com.bms.entity.Account;
import com.bms.entity.User;

@Controller
public interface UserRepository extends JpaRepository<User, Long>{
	Optional<User> findByPhone(String phone);
	Optional<User> findByEmail(String email);
	Optional<User> findByAccount(Account account);
	
	Page<User> findAll(Pageable pageable);
}

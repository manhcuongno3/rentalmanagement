package com.bms.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.bms.entity.Constants;
import com.bms.entity.ReportPost;
import com.bms.repository.ReportPostRepository;

@Service
public class ReportPostService {
	@Autowired
	ReportPostRepository reportPostRepository;
	
	public Page<ReportPost> findAllReport(int pageNum){
		Pageable pageable = PageRequest.of(pageNum-1, Constants.PAGE_SIZE);
		return reportPostRepository.findAll(pageable);
	}
}

package com.bms.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.bms.entity.Category;
import com.bms.entity.Constants;
import com.bms.entity.District;
import com.bms.entity.Post;
import com.bms.entity.User;
import com.bms.repository.PostRepository;

@Service
public class PostService {
	@Autowired
	PostRepository postRepository;
	
	public Page<Post> findAllOrderByDesc(int pageNum,Category category, District district
			,Double startArea, Double endArea, Double startPrice, Double endPrice){
		Pageable pageable = PageRequest.of(pageNum-1, Constants.PAGE_SIZE);
		
		
		if(district!=null) {
			return postRepository.findByDistrictAndCategoryAndAreaBetweenAndPriceBetweenAndIsApproveTrueOrderByIdDesc(district,category,  startArea, endArea, startPrice, endPrice, pageable);
		}
		return postRepository.findByIsApproveTrueOrderByIdDesc(pageable);
	}
	
	public boolean insertPost(Post post) {
		return postRepository.save(post)!=null;
	}
	
	public Post findById(long id) {
		return postRepository.findById(id).get();
	}
	
	public Page<Post> findAllNonApprovePost(int pageNum) {
		Pageable pageable = PageRequest.of(pageNum-1, Constants.PAGE_SIZE);
		return postRepository.findByIsApproveFalseAndIsHiddenFalse(pageable);
	}
	
	public Page<Post> findAllHiddenPost(int pageNum) {
		Pageable pageable = PageRequest.of(pageNum-1, Constants.PAGE_SIZE);
		return postRepository.findByIsHiddenTrue(pageable);
	}
	
	public Page<Post> findWaitApproveUserPost(int pageNum, User user) {
		Pageable pageable = PageRequest.of(pageNum-1, Constants.PAGE_SIZE);
		return postRepository.findByUserAndIsApproveFalse(user, pageable);
	}
	
	public Page<Post> findApproveUserPost(int pageNum, User user) {
		Pageable pageable = PageRequest.of(pageNum-1, Constants.PAGE_SIZE);
		return postRepository.findByUserAndIsApproveTrue(user, pageable);
	}
	
	public Page<Post> findHiddenUserPost(int pageNum, User user) {
		Pageable pageable = PageRequest.of(pageNum-1, Constants.PAGE_SIZE);
		return postRepository.findByUserAndIsHiddenTrue(user, pageable);
	}
	
	public Page<Post> findbyUser(int pageNum, User user) {
		Pageable pageable = PageRequest.of(pageNum-1, Constants.PAGE_SIZE);
		return postRepository.findByUser(user, pageable);
	}
	
}

package com.bms.entity;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "report_post")
@AssociationOverrides({
@AssociationOverride(name = "pk.user", joinColumns = @JoinColumn(name = "user_id")),
@AssociationOverride(name = "pk.post", joinColumns = @JoinColumn(name = "post_id"))
})
public class ReportPost {

	@EmbeddedId
	private ReportPostId pk = new ReportPostId();
	
	@Column(length = 400)
	private String content;
	
	public ReportPost() {
	
	}

	public ReportPostId getPk() {
		return pk;
	}

	public void setPk(ReportPostId pk) {
		this.pk = pk;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}

package com.bms.entity;

public class Constants {
	public static final String ROLE_USER_OWN = "ROLE_OWN";
	public static final String ROLE_USER_TENANT = "ROLE_TENANT";
	public static final byte PAGE_SIZE = 3;
}

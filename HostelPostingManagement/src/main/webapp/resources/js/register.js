function isOkPass() {
	const passWord = document.getElementById("password").value;
	const pattern_password = "^(?=.*[a-z]{1,})(?=.*[A-Z]{1,})(?=.*[0-9]{1,})(?=.*[!@#\$%\^&\*]{1,})(?=.{8,})";
	const isOkPassWord = passWord.match(pattern_password);
	console.log("ok")
	if (passWord.length < 8) {
		swal({
			title: "Warning!",
			text: "Your pass word must have minimum 8 characters !",
			icon: "warning",
		});
		return false
	}

	if (!isOkPassWord) {
		swal({
			title: "Warning!",
			text: "Your pass word must have minimum 1 lowercase, 1 uppercase, 1 special characters and 1 number!",
			icon: "warning",
		});
		return false
	}
	return true
} 
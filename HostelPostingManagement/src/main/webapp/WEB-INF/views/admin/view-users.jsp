
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

<form>
	<div class="container">
		<h1>Danh sách User</h1>
		<hr>
		<div class="border rounded">
			<div style="background-color: rgb(237, 240, 243);">
				<h6 class="pt-3 ml-3 ">Danh sách user</h6>
				<hr>
			</div>
			<div class="container">
				<table class="table table-bordered table table-striped">
					<thead>
						<tr>
							<th class="col">Họ và tên</th>
							<th class="col">Địa chỉ</th>
							<th class="col">Ngày tạo</th>
							<th class="col">Số điện thoại</th>
							<th class="col">Email</th>
							<th class="col">UserName</th>
							<th class="col"></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="user" items="${listProducts}">
							<c:url var="blockLink" value="/block-user">
								<c:param name="userId" value="${user.id}"></c:param>
							</c:url>
							<tr>
								<td>${user.fullName}</td>
								<td>${user.address}</td>
								<td>${user.createdDate}</td>
								<td>${user.phone}</td>
								<td>${user.email}</td>
								<td>${user.account.username}</td>
								<td><a class="btn btn-outline-secondary mr-1 btn-sm"
									href="${blockLink}"><i class="bi bi-x-circle text-danger"></i></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div>&nbsp;</div>
				<c:if test="${totalPages > 1}">
					<div class="text-right">
						Total: ${totalItems} &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;
						<c:choose>
							<c:when test="${currentPage >1}">
								<a href="1">Fist</a>
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${currentPage >1}">
								<a href="${currentPage -1}"><i
									class="bi bi-arrow-left-circle"></i></a>&nbsp;
							</c:when>
						</c:choose>

						<c:forEach begin="1" end="${totalPages}" var="i">
							<c:choose>
								<c:when test="${currentPage != i}">
									<a href="${contextPath}/view-user/page/${i}">[${i}]</a>&nbsp;
								</c:when>
								<c:otherwise>
									<span>[${i}]</span>&nbsp;
								</c:otherwise>
							</c:choose>
						</c:forEach>

						<c:choose>
							<c:when test="${currentPage < totalPages}">
								<a href="${contextPath}/view-user/page/${currentPage+1}"><i
									class="bi bi-arrow-right-circle"></i></a>&nbsp;
							</c:when>
						</c:choose>

						<c:choose>
							<c:when test="${currentPage < totalPages}">
								<a href="${contextPath}/view-user/page/${totalPages}">Last</a>&nbsp;
							</c:when>
						</c:choose>
					</div>
				</c:if>
			</div>
		</div>
	</div>
</form>

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<div class="text-center m-auto">
	<form action="reset-password" method="post">
		<div class="container">
			<h1>Thông tin cá nhân</h1>
			<hr>
			<div class="border rounded">
				<div style="background-color: rgb(237, 240, 243);">
					<h6 class="pt-3 ml-3 ">Cập nhật mật khẩu</h6>
					<c:if test="${not empty error}">
						<div class="text-danger bg-light ml-2">
							<c:out value="${error}"></c:out>
							<br />
						</div>
					</c:if>
					<hr>

				</div>
				<div class="container">
					<div class="mb-3">
						<label class="form-label" style="font-weight: bold">Nhập
							mật khẩu cũ chỉ</label> <input type="text" class="form-control"
							name="passwordOld" placeholder="Enter the passwordOld"
							required="required">
					</div>

					<div class="mb-3">
						<label class="form-label" style="font-weight: bold">Nhập
							mật khẩu mới chỉ</label> <input type="text" class="form-control"
							name="passwordOld" placeholder="Enter the password"
							required="required">
					</div>

					<div class="mb-3">
						<label class="form-label" style="font-weight: bold">Nhập
							lại mật khẩu mới chỉ</label> <input type="text" class="form-control"
							name="passwordOld" placeholder="Enter the password"
							required="required">
					</div>

					<div class="mb-3">
						<button type="submit" class="btn btn-outline-secondary">Lưu</button>
						<button type="button" class="btn btn-outline-secondary">Reset
						</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<div class="mt-3">
	<c:choose>
		<c:when test="${not empty editPost}">
			<h3>Chỉnh sửa bài đăng</h3>
		</c:when>
		<c:otherwise>
			<h3>Thêm bài đăng mới</h3>
		</c:otherwise>
	</c:choose>
	<hr>
</div>
<c:if test="${not empty notify}">
	<div class="text-danger">${notify}</div>
</c:if>

<form class="pl-5 mt-3 mb-3 row" method="post" action="add-product"
	enctype="multipart/form-data">
	<div class="col-6 ">
		<div class="row mb-3">
			<label for="inputName" class="col-sm-2 col-form-label">Tiêu
				đề </label>
			<div class="col-sm-10">
				<textarea class="form-control" name="title" id="title" rows="2"
					required="required"></textarea>
			</div>
		</div>

		<div class="row mb-3">
			<label for="price" class="col-sm-2 col-form-label">Mô tả</label>
			<div class="col-sm-10">
				<textarea class="form-control" name="shortDescription"
					id="shortDescription" rows="2" required="required"></textarea>
			</div>
		</div>

		<div class="row mb-3">
			<label class="col-sm-2 col-form-label">Danh mục</label>
			<div class="col-sm-10">
				<select class="form-select w-100 border-primary p-2 rounded"
					name="categoryId">
					<c:forEach var="category" items="${listCategories}">
						<option value="${category.id}">${category.name}</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="row mb-3">
			<label class="col-sm-2 col-form-label">Vị trí</label>
			<div class="col-sm-10">
				<select class="form-select w-100 border-primary p-2 rounded"
					name="districtId">
					<c:forEach var="district" items="${listDistricts}">
						<option value="${district.id}">${district.name}</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="row mb-3">
			<label for="waterPrice" class="ol-sm-2 col-form-label">Giá
				nước (m<sup>2</sup>)
			</label>
			<div class="col-sm-10">
				<input type="number" name="waterPrice" class="form-control"
					id="waterPrice" required="required">
			</div>
		</div>

		<div class="row mb-3">
			<label class="col-sm-2 col-form-label">Điều hòa</label>
			<div class="col-sm-10">
				<select class="form-select w-100 border-primary p-2 rounded"
					name="airCondittion">
					<option value="true">Có</option>
					<option value="false">Không</option>
				</select>
			</div>
		</div>

		<div class="row mb-3">
			<label class="col-sm-2 col-form-label">Truyền hình cáp</label>
			<div class="col-sm-10">
				<select class="form-select w-100 border-primary p-2 rounded"
					name="cableTv">
					<option value="true">Có</option>
					<option value="false">Không</option>
				</select>
			</div>
		</div>

		<div class="row mb-3">
			<label class="col-sm-2 col-form-label">Máy nước nóng</label>
			<div class="col-sm-10">
				<select class="form-select w-100 border-primary p-2 rounded"
					name="heater">
					<option value="true">Có</option>
					<option value="false">Không</option>
				</select>
			</div>
		</div>

		<div class="row mb-3">
			<label class="col-sm-2 col-form-label">Chọn hình ảnh</label>
			<div class="col-sm-10">
				<div class="row mb-3">
					<div class="col-sm-10">
						<input class="form-control " value="Upload File" type="file"
							name="file" id="file" accept="image/*" required="required"
							multiple="multiple">
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="col-6">
		<div class="row mb-3">
			<label for="content" class="col-sm-2 col-form-label">Nội
				dung </label>
			<div class="col-sm-10">
				<textarea class="form-control" name="content" id="content" rows="2"
					required="required"></textarea>
			</div>
		</div>

		<div class="row mb-3">
			<label for="address" class="col-sm-2 col-form-label">Địa
				chỉ</label>
			<div class="col-sm-10">
				<textarea class="form-control" name="address" id="address" rows="2"
					required="required"></textarea>
			</div>
		</div>

		<div class="row mb-3">
			<label for="price" class="col-sm-2 col-form-label">Giá thuê</label>
			<div class="col-sm-10">
				<input type="number" name="price" class="form-control" id="price"
					required="required">
			</div>
		</div>

		<div class="row mb-3">
			<label for="area" class="col-sm-2 col-form-label">Diện tích
				sử dụng </label>
			<div class="col-sm-10">
				<input type="number" name="area" class="form-control" id="area"
					required="required">
			</div>
		</div>

		<div class="row mb-3">
			<label for="electricPrice" class="col-sm-2 col-form-label">Giá
				điện (KWh)</label>
			<div class="col-sm-10">
				<input type="number" name="electricPrice" class="form-control"
					id="electricPrice" required="required">
			</div>
		</div>

		<div class="row mb-3">
			<label class="col-sm-2 col-form-label">Kết nối internet</label>
			<div class="col-sm-10">
				<select class="form-select w-100 border-primary p-2 rounded"
					name="internet">
					<option value="true">Có</option>
					<option value="false">Không</option>
				</select>
			</div>
		</div>

		<div class="row mb-3">
			<label class="col-sm-2 col-form-label">Chỗ để xe</label>
			<div class="col-sm-10">
				<select class="form-select w-100 border-primary p-2 rounded"
					name="parking">
					<option value="true">Có</option>
					<option value="false">Không</option>
				</select>
			</div>
		</div>

		<div class="row mb-3">
			<label class="col-sm-2 col-form-label">Còn phòng</label>
			<div class="col-sm-10">
				<select class="form-select w-100 border-primary p-2 rounded"
					name="status">
					<option value="true">Có</option>
					<option value="false">Không</option>
				</select>
			</div>
		</div>

	</div>

	<button type="submit" class="btn btn-primary">Đăng bài</button>
</form>